<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminEventsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function admins_can_visit_the_admin_events_page()
    {

        $this->actingAsAdmin()//conectado como admin
        ->get(route('admin_events'))
            ->assertStatus(200)//se espera una respuesta OK
            ->assertSee('Admin Events'); //se espera ver el texto Admin Panel
    }

    /** @test */
    function non_admin_users_cannot_visit_the_admin_events_page()
    {


        $this->actingAsUser()//conectado como usuario
        ->get(route('admin_events'))
            ->assertStatus(302) //403: conectado, ruta existe pero tiene permisos
            ->assertRedirect('login');
    }

    /** @test */
    function guests_cannot_visit_the_admin_events_page()
    {
        $this
            ->get(route('admin_events'))
            ->assertStatus(302)
            ->assertRedirect('login');
    }
}
